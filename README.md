# delightful c software [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of of lightweight, cross-platform C programs and libraries.  All programs are Free, Libre and/or Open Source with source code available.  They're lightweight and cross-platform compatible with minimal dependencies, so they should be easy to build from source on most operating systems with a C compiler.

Be sure to check out other [Delightful Lists](https://codeberg.org/teaserbot-labs/delightful).

## Contents

- [List](#list)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## List

| Program | Summary|
| :---: | --- |
|[bard](https://github.com/festvox/bard)|Ebook reader with text-to-speech support using SDL and flite.  I have some patches for this program to improve portability and support using SDL2.|
|[BearSSL](https://bearssl.org/)|Rather secure implementation of the SSL/TLS protocol.  Can be used with curl.|
|[branchless-utf8](https://github.com/skeeto/branchless-utf8)|Decodes a UTF-8 code point without any if statements, loops, short-circuit operators or other conditional jumps.|
|[cal](http://unicorn.us.com/cal.html)|Command line calendar.|
|[cDetect](http://cdetect.sourceforge.net/)|C based alternative to GNU configure/autoconf.  More info at IngwiePhoenix's [cDetect project](https://github.com/IngwiePhoenix/cDetect).  Plus, I've forked the project and use it with many of my builds from source code.  I've added support for cross-compiling and many, many other features.  Contact me if you'd like a copy.  I currently have an [older version](http://www.distasis.com/cpp/lmports.htm) of my modifications uploaded.|
|[csvutils](https://github.com/rgamble/csvutils)|CSV command utilities.  Uses libcsv.|
|[curl](https://curl.se/)|Command line data transfer tool.|
|[diction and style](https://www.gnu.org/software/diction/)|Analyze documents for readability and other metrics.  Find grammatical issues.|
|[diff](https://lists.suckless.org/dev/1601/28247.html)|diff implementation for sbase.  Check the follow-up mailing list threads for further patches.|
|[diffh](https://sourceforge.net/projects/diffh/)|Works with diff and creates an easy to read display of differences between files in HTML format.|
|[dr_libs](https://github.com/mackron/dr_libs)|Single file header audio decoding libraries.|
|[fcurl](https://github.com/curl/fcurl)|Library to simplify working with curl.|
|[BSD gettext](https://www.mmnt.net/db/0/18/ftp.khstu.ru/pub/unix/distfiles)|Older BSD gettext/libintl implementation.  I have a fork of this one as well.  Also, check out the BSD Citrus Project.|
|[BSD gzip](https://github.com/NetBSD/src/tree/trunk/usr.bin/gzip)|BSD version of the gzip compression/decompression program.  There are various forks to port this to operating systems other than BSD.  I have a portable fork as well.|
|[gifsicle](http://www.lcdf.org/gifsicle/)|GIF animator utility.|
|[grafx2](http://grafx2.chez.com/)|Graphics editor using SDL.|
|[is_utf8](https://github.com/JulienPalard/is_utf8)|Check if a given string is a valid UTF-8 format.|
|[less](https://www.greenwoodsoftware.com/less/)|Less is more than more, pager program.|
|[libarchive](https://www.libarchive.org/)|Archive and compression library, bsdtar and bsdcpio.|
|[libcsv](https://github.com/rgamble/libcsv)|ANSI C library to read and write CSV files.|
|[liblfds](https://www.liblfds.org/)|Portable license-free, lock-free data structure library.|
|[libgrapheme](https://libs.suckless.org/libgrapheme/)|C99 Unicode library including encoding, decoding and line-break functionality.|
|[liblzw](https://github.com/vapier/liblzw)|Library for LZW (.Z) decompression.|
|[libtomcrypt](https://github.com/libtom/libtomcrypt)|Public Domain cryptography library.|
|[libutf](https://github.com/cls/libutf)|C89 UTF-8 library which includes an API compatible with Plan 9's libutf plus a number of improvements.|
|[lxsplit](http://lxsplit.sourceforge.net/)|Command line file split/join tool.|
|[man](https://github.com/jbruchon/elks/blob/1b6110b73fbb123021a5a29b05d8fa9caef33235/elkscmd/sys_utils/man.c)|C program to view standard man pages.  Now part of Elks (elkscmd/sys_utils).|
|[man](https://github.com/rofl0r/hardcore-utils/blob/master/man.c)|Fork of the man program used by Elks.|
|[mandoc](http://mandoc.bsd.lv/)|BSD version of man page utilities.  Uses their manpage format.  Includes tools to convert to manpage format used by most man tools.|
|[mandoc](https://embedeo.org/ws/doc/man_windows/)|Windows port of older version of mandoc.|
|[minzip](http://zlib.net/)|A zip library for zlib. Useful when working with files in zip format. Code is in the contrib section of zlib.|
|[monocypher](https://github.com/LoupVaillant/monocypher)|An easy to use, easy to deploy, auditable crypto library written in portable C.|
|[nanosvg](https://github.com/memononen/nanosvg)|Lightweight SVG library.|
|[nanotodon](https://github.com/taka-tuos/nanotodon)|Lightweight TUI/C99 Mastodon client which uses ncurses (or pdcurses) and curl.|
|[ncurses hexedit](http://www.rogoyski.com/adam/programs/hexedit/)|Curses based hex editor.|
|[nemini](https://github.com/neonmoe/nemini)|Lightweight SDL2 based Gemini client.|
|[BSD patch](https://github.com/openbsd/src/tree/master/usr.bin/patch)|BSD fork of the patch program.  I have a fork of this as well with some portability additions to better handle carriage return/line feed issues.|
|[pdfconcat](https://github.com/pts/pdfconcat)|Concatenates PDF files.|
|[pdftxt](https://litcave.rudi.ir/)|Convert PDF to text. Helpful for searching PDFs with grep.|
|[pkgconf](https://github.com/pkgconf/pkgconf)|Drop in replacement for pkg-config with no circular dependencies.|
|[picaxo](http://gigi.nullneuron.net/comp/picaxo/)|Graphics viewer using SDL or SDL2.  I have patches to add SDL2 support.  Contact me if you'd like a copy.|
|[pspg](https://github.com/okbob/pspg)|Postgres pager provides a console based pager for PostgreSQL, MySQL, CSV and other formats.  Uses ncurses (or pdcurses).|
|[sbase](https://core.suckless.org/sbase/)|Efficient implementations of core base utilities.|
|[sc](https://github.com/tezc/sc)|C99 small portable libraries and data structures.|
|[shot](https://github.com/rr-/shot/)|Command line screenshot program.|
|[sox](http://sox.sourceforge.net/)|Sound exchange utility and library. Converts sound formats. Plays audio files.|
|[stb](https://github.com/nothings/stb)|Public Domain single file header libraries.  Includes stb_truetype.h, a lightweight alternative to the freetype library, which can parse, decode and rasterize characters from truetype fonts.
|[tgcrypt](https://www.ttgurney.com/tgcrypt.html)|Small encryption package that does file encryption and full-disk encryption on Linux.  Works with monocypher.|
|[unarr](https://github.com/sumatrapdfreader/sumatrapdf/tree/master/ext/unarr)|Decompression library for rar and other formats. Part of SumatraPDF project.|
|[utf.8](https://github.com/sheredom/utf8.h)|Single header UTF-8 string functions.|
|[utf8proc](https://github.com/JuliaStrings/utf8proc)|Small UTF-8 encoding library that provides Unicode normalization, case-folding and other operations.|
|[x509cert](https://github.com/michaelforney/x509cert)|Generate x509 certificate requests. Works with BearSSL.|

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/lmemsm/delightful-c-software/issues) with the tracker.

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/master/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC by 4.0](https://licensebuttons.net/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)
