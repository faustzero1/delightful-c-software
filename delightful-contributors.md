# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delightful project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- [Laura Michaels](https://www.distasis.com) (codeberg: [@lmemsm](https://codeberg.org/lmemsm), dreamwidth: [journal entries](https://lmemsm.dreamwidth.org/))